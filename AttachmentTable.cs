﻿using System;
using System.Collections.Generic;

namespace TestingSonarcloud;

public partial class AttachmentTable
{
    public int AttachmentId { get; set; }

    public string? CreatedBy { get; set; }

    public string? AttOriginalName { get; set; }

    public string? AttUrl { get; set; }

    public string? AttRandomName { get; set; }

    public string? AttContentType { get; set; }

    public int? AttachmentSize { get; set; }

    public string? AttParam1 { get; set; }

    public string? AttParam2 { get; set; }

    public string? AttDescription { get; set; }

    public int? RequestId { get; set; }

    public int? ExceptionId { get; set; }

    public string? Status { get; set; }

    public DateTime? DateCreated { get; set; }

    public DateTime? DateModified { get; set; }

    public int? Bcdrid { get; set; }
}
