﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TestingSonarcloud;
using TestingSonarcloud.Repository.Contracts;

namespace BlazorApp4.Server.Controllers
{
    [Route("api/attach")]
    [ApiController]
    public class AttachmentTableController : ControllerBase
    {
        private readonly IAttachmentRepository attachmentRepository;

        public AttachmentTableController(IAttachmentRepository attachmentRepository)
        {
            this.attachmentRepository = attachmentRepository;
        }

       
        [HttpGet]
        public async Task<ActionResult<IEnumerable<AttachmentTable>>> GetItems()
        {
            try
            {
                var list = await this.attachmentRepository.GetItems();


                if (list == null)
                {
                    return NotFound();
                }
                else if (list.Count() == 0)
                {
                    return NoContent(); ;
                }
                {
                    //var LookuplistDtos = lookuplist.ConvertToDto();

                    return Ok(list);
                }

            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                                "Error retrieving data from the database");

            }
        }


        [HttpGet("{id}")]
        public async Task<ActionResult<AttachmentTable>> Get(int id)
        {
            var item = await this.attachmentRepository.GetItem(id);
            return Ok(item);
        }

       
        [HttpPost]
        public async Task<ActionResult<AttachmentTable>> Post([FromBody] AttachmentTable attachmenttable)
        {
            var item = await attachmentRepository.AddAttachment(attachmenttable);
            return Ok(item);
        }

        

        [HttpPut("{id}")]
        public async Task<AttachmentTable> Put(int id, [FromBody] AttachmentTable attachmenttable)
        {
            var item = await attachmentRepository.UpdateAttachment(id, attachmenttable);
            return item;
        }


        [HttpDelete("{id}")]
        public async Task<ActionResult<AttachmentTable>> Delete(int id)
        {
            var item = await attachmentRepository.DeleteAttachment(id);
            return Ok(item);
        }

        [HttpGet("GetItemsByRequestID/{RequestID}")]
        public async Task<ActionResult<IEnumerable<AttachmentTable>>> GetItemsByRequestID(int RequestID)
        {
            var item = await this.attachmentRepository.GetItemsByRequestID(RequestID);
            return Ok(item);
        }


        [HttpGet("GetItemsCreatedBy/{UserID}")]
        public async Task<ActionResult<IEnumerable<AttachmentTable>>> GetItemsCreatedBy(string UserID)
        {
            var item = await this.attachmentRepository.GetItemsCreatedBy(UserID);
            return Ok(item);
        }

    }
}
