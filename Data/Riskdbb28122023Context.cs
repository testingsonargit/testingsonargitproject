﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace TestingSonarcloud.Data;

public partial class Riskdbb28122023Context : DbContext
{
    public Riskdbb28122023Context()
    {
    }

    public Riskdbb28122023Context(DbContextOptions<Riskdbb28122023Context> options)
        : base(options)
    {
    }

    public virtual DbSet<AttachmentTable> AttachmentTables { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
        => optionsBuilder.UseSqlServer("Data Source=HAMS\\SQLEXPRESS;Initial Catalog=riskdbb28122023;Integrated Security=True;Encrypt=True;TrustServerCertificate=True;");

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<AttachmentTable>(entity =>
        {
            entity.HasKey(e => e.AttachmentId);

            entity.ToTable("AttachmentTable");

            entity.Property(e => e.AttachmentId).HasColumnName("AttachmentID");
            entity.Property(e => e.AttContentType).HasMaxLength(100);
            entity.Property(e => e.AttDescription).HasMaxLength(1000);
            entity.Property(e => e.AttOriginalName).HasMaxLength(250);
            entity.Property(e => e.AttParam1).HasMaxLength(250);
            entity.Property(e => e.AttParam2).HasMaxLength(250);
            entity.Property(e => e.AttRandomName).HasMaxLength(250);
            entity.Property(e => e.AttUrl)
                .HasMaxLength(1000)
                .HasColumnName("AttURL");
            entity.Property(e => e.Bcdrid).HasColumnName("BCDRID");
            entity.Property(e => e.CreatedBy).HasMaxLength(100);
            entity.Property(e => e.ExceptionId).HasColumnName("ExceptionID");
            entity.Property(e => e.RequestId).HasColumnName("RequestID");
            entity.Property(e => e.Status)
                .HasMaxLength(20)
                .HasColumnName("STATUS");
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
