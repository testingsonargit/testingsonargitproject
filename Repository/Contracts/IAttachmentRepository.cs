﻿
using System.Runtime.InteropServices;

namespace TestingSonarcloud.Repository.Contracts
{
    public interface IAttachmentRepository
    {
        Task<IEnumerable<AttachmentTable>> GetItems();
        Task<AttachmentTable> GetItem(int id);
        Task<AttachmentTable> AddAttachment(AttachmentTable entity);
        Task<AttachmentTable> UpdateAttachment(int id, AttachmentTable entity);
        Task<IEnumerable<AttachmentTable>> GetItemsCreatedBy(string CreatedByUserID);
        Task<IEnumerable<AttachmentTable>> GetItemsByRequestID(int RequestID);
        Task<AttachmentTable> DeleteAttachment(int id);
    }
}
